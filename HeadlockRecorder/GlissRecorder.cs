﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadlockRecorder
{
    /// <summary>
    /// To create the feedback observed when a user is approaching a door without veering in sonification mode, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=glissando.wav -o=true
    /// 
    /// To customize the frequencies and amplitudes of the glissando, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=glissando.wav -o=true --start=130.0 --end=1050.0 --ampstart=0.50 --ampend=0.05
    /// </summary>
    public class GlissRecorder : Recorder
    {
        public RecordingSettings Settings { get; set; }

        public WaveFormat Format { get; private set; }

        public GlissRecorder(RecordingSettings s)
        {
            Settings = s;
        }

        public void Record()
        {
            Format = new WaveFormat(Settings.Rate, 1);

            using (WaveFileWriter writer = new WaveFileWriter(Settings.Filename, Format))
            {
                int samples = (int)(Settings.Seconds * Settings.Rate);

                double angle = 0.0;
                double freq, ampl;

                short[] buf = new short[1];

                double fRange = Settings.FreqEnd - Settings.FreqStart;
                double aRange = Settings.AmpEnd - Settings.AmpStart;
                double iFactor = (2 * Math.PI / Settings.Rate);

                for (int i = 0; i < samples; ++i)
                {
                    double percent = ((double)i / samples);

                    freq = (Settings.FreqStart + fRange * percent);
                    ampl = (Settings.AmpStart + aRange * percent);

                    buf[0] = (short)(short.MaxValue * ((double)Math.Sin(angle)) * ampl);
                    writer.WriteSamples(buf, 0, 1);
                    angle = (angle + iFactor * freq) % (2.0 * Math.PI);
                }
            }
        }
    }
}
