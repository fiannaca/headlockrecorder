﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.AudioFormat;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace HeadlockRecorder
{
    public class SuccessRecorder : Recorder
    {
        public RecordingSettings Settings { get; set; }

        public WaveFormat Format;

        public SuccessRecorder(RecordingSettings s)
        {
            Settings = s;
        }

        public void Record()
        {
            short[] soundfx = GetWavData("res\\success.wav");
            short[] tts = GetTtsWavData("You have arrived.");

            using (WaveFileWriter writer = new WaveFileWriter(Settings.Filename, Format))
            {
                writer.WriteSamples(soundfx, 0, soundfx.Length);
                writer.WriteSamples(tts, 0, tts.Length);
            }
        }

        private short[] GetTtsWavData(string phrase)
        {
            string tmp = "tmp.wav";

            using (SpeechSynthesizer sp = new SpeechSynthesizer())
            {
                sp.SelectVoiceByHints(VoiceGender.Female);

                sp.SetOutputToWaveFile(
                    tmp,
                    new SpeechAudioFormatInfo(Settings.Rate, AudioBitsPerSample.Sixteen, AudioChannel.Mono)
                );

                PromptBuilder builder = new PromptBuilder();
                builder.AppendText(phrase);

                sp.Rate = 4;
                sp.Speak(builder);
            }

            short[] WavData;

            using (WaveFileReader reader = new WaveFileReader(tmp))
            {
                Format = reader.WaveFormat;

                byte[] buffer = new byte[reader.Length];
                int read = reader.Read(buffer, 0, buffer.Length);

                WavData = new short[read / 2];
                Buffer.BlockCopy(buffer, 0, WavData, 0, read);
            }

            File.Delete(tmp);

            return WavData;
        }

        private short[] GetWavData(string res)
        {
            short[] WavData;

            using (WaveFileReader reader = new WaveFileReader(res))
            {
                Format = reader.WaveFormat;

                byte[] buffer = new byte[reader.Length];
                int read = reader.Read(buffer, 0, buffer.Length);

                WavData = new short[read / 2];
                Buffer.BlockCopy(buffer, 0, WavData, 0, read);
            }

            return WavData;
        }

        private int MsToSamples(int milliseconds, WaveFormat format)
        {
            //Get the number of bytes
            int bytes = (int)(milliseconds * (format.AverageBytesPerSecond / 1000.0));

            //Ensure that the silence length is a multiple of the block align
            bytes -= bytes % format.BlockAlign;

            //Convert from bytes to shorts
            return bytes / 2;
        }
    }
}
