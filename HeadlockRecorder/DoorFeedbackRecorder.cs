﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Synthesis;
using System.Speech.AudioFormat;
using System.IO;

namespace HeadlockRecorder
{
    /// <summary>
    /// To create the feedback when a door is observed in sonification mode, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=door.wav -h=true
    ///     
    /// To create the feedback when a door is NOT observed in sonification mode, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=nodoor.wav -h=false
    ///     
    /// To create the feedback when a door is observed in TTS mode, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=ttsdoor.wav -t=true -h=true
    ///     
    /// To create the feedback when a door is NOT observed in TTS mode, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=ttsnodoor.wav -t=true -h=false
    /// </summary>
    public class DoorFeedbackRecorder : Recorder
    {
        public RecordingSettings Settings { get; set; }

        int SilenceMs;

        public DoorFeedbackRecorder(RecordingSettings s)
        {
            Settings = s;
        }

        public void Record()
        {
            if(Settings.Feedback == FeedbackTypes.Sonification)
            {
                SilenceMs = 1500;

                if(Settings.Mode == FeedbackModes.DoorFound)
                {
                    RecordDoorSound("res\\door.wav");
                }
                else
                {
                    RecordDoorSound("res\\nodoor.wav");
                }
            }
            else
            {
                SilenceMs = 1000;

                if (Settings.Mode == FeedbackModes.DoorFound)
                {
                    RecordTts("Door Found");
                }
                else
                {
                    RecordTts("No Door");
                }
            }
        }

        private void RecordDoorSound(string res)
        {
            WaveFormat format;
            short[] doorSound;

            using (WaveFileReader reader = new WaveFileReader(res))
            {
                format = reader.WaveFormat;

                byte[] buffer = new byte[reader.Length];
                int read = reader.Read(buffer, 0, buffer.Length);

                doorSound = new short[read / 2];
                Buffer.BlockCopy(buffer, 0, doorSound, 0, read);
            }

            using (WaveFileWriter writer = new WaveFileWriter(Settings.Filename, format))
            {
                int remaining = (int)(Settings.Seconds * (format.AverageBytesPerSecond / 2.0));

                int SilenceLen = MsToSamples(SilenceMs, format);

                while (remaining > 0)
                {
                    writer.WriteSamples(doorSound, 0, doorSound.Length);

                    remaining -= doorSound.Length;

                    if (remaining > 0)
                    {
                        if (remaining < SilenceLen)
                        {
                            SilenceLen -= SilenceLen - remaining;
                        }

                        short[] silence = new short[SilenceLen];
                        writer.WriteSamples(silence, 0, silence.Length);

                        remaining -= silence.Length;
                    }
                }
            }
        }

        private void RecordTts(string phrase)
        {
            string tmp = "tmp.wav";

            using(SpeechSynthesizer sp = new SpeechSynthesizer())
            {
                sp.SelectVoiceByHints(VoiceGender.Female);

                sp.SetOutputToWaveFile(
                    tmp, 
                    new SpeechAudioFormatInfo(Settings.Rate, AudioBitsPerSample.Sixteen, AudioChannel.Mono)
                );

                PromptBuilder builder = new PromptBuilder();
                builder.AppendText(phrase);

                sp.Rate = 4;
                sp.Speak(builder);
            }

            RecordDoorSound(tmp);

            File.Delete(tmp);
        }

        private int MsToSamples(int milliseconds, WaveFormat format)
        {
            //Get the number of bytes
            int bytes = (int)(milliseconds * (format.AverageBytesPerSecond / 1000.0));

            //Ensure that the silence length is a multiple of the block align
            bytes -= bytes % format.BlockAlign;

            //Convert from bytes to shorts
            return bytes / 2;
        }
    }
}
