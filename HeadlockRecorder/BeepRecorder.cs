﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace HeadlockRecorder
{
    /// <summary>
    /// To create the feedback observed when veering is increasing (over a period of 5 seconds), execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=veerincrease.wav -o=false -i=true --delay=200 --delta=100 
    ///     
    /// To create the feedback observed when veering is decreasing, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=veerdecrease.wav -o=false -d=true --delay=1000 --delta=100 --min=200
    /// </summary>
    public class BeepRecorder : Recorder
    {
        public RecordingSettings Settings { get; set; }

        public BeepRecorder(RecordingSettings s)
        {
            Settings = s;
        }

        public void Record()
        {
            WaveFormat format;
            short[] beep;

            using(WaveFileReader reader = new WaveFileReader("res\\beep.wav"))
            {
                format = reader.WaveFormat;

                byte[] buffer = new byte[reader.Length];
                int read = reader.Read(buffer, 0, buffer.Length);

                beep = new short[read / 2];
                Buffer.BlockCopy(buffer, 0, beep, 0, read);
            }

            using(WaveFileWriter writer = new WaveFileWriter(Settings.Filename, format))
            {
                int remaining = (int)(Settings.Seconds * (format.AverageBytesPerSecond / 2.0));

                int Min = MsToSamples(Settings.MinDelay, format);
                int BaseSilenceLen = MsToSamples(Settings.InitialDelay, format);
                int Delta = GetDelta(format);

                while(remaining > 0)
                {
                    writer.WriteSamples(beep, 0, beep.Length);

                    remaining -= beep.Length;

                    if (remaining > 0)
                    {
                        int len = Math.Max(BaseSilenceLen + Delta, Min);

                        while(remaining < len)
                        {
                            len--;
                        }

                        short[] silence = new short[len];
                        writer.WriteSamples(silence, 0, silence.Length);

                        Delta += Delta;
                        remaining -= silence.Length;
                    }
                }
            }
        }

        private int MsToSamples(int milliseconds, WaveFormat format)
        {
            //Get the number of bytes
            int bytes = (int)(milliseconds * (format.AverageBytesPerSecond / 1000.0));

            //Ensure that the silence length is a multiple of the block align
            bytes -= bytes % format.BlockAlign; 

            //Convert from bytes to shorts
            return bytes / 2;
        }

        private int GetDelta(WaveFormat format)
        {
            if(Settings.Increasing)
            {
                return MsToSamples(Settings.DelayDelta, format);
            }
            else if(Settings.Decreasing)
            {
                return -MsToSamples(Settings.DelayDelta, format);
            }
            else
            {
                return 0;
            }
        }
    }
}
