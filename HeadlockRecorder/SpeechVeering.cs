﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.AudioFormat;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace HeadlockRecorder
{
    /// <summary>
    /// To create the tts veering feedback when a user is veering to the right, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=veerincrease.wav -t=true -o=false -i=true --delay=0 --delta=125
    ///     
    /// To create the tts veering feedback when a user is veering to the right, execute:
    /// 
    ///     HeadlockRecorder.exe -s=5 -f=veerincrease.wav -t=true -o=false -i=true --delay=0 --delta=125 --left=true
    /// </summary>
    public class SpeechVeering : Recorder
    {
        public RecordingSettings Settings { get; set; }

        WaveFormat Format;

        public SpeechVeering(RecordingSettings s)
        {
            Settings = s;

            Format = new WaveFormat(Settings.Rate, 1);
        }

        public void Record()
        {
            short[] tts = GetTtsWavData(Settings.VeeringLeft ? "Left" : "Right");

            using (WaveFileWriter writer = new WaveFileWriter(Settings.Filename, Format))
            {
                int remaining = (int)(Settings.Seconds * (Format.AverageBytesPerSecond / 2.0));

                int Min = MsToSamples(Settings.MinDelay, Format);
                int BaseSilenceLen = MsToSamples(Settings.InitialDelay, Format);
                int Delta = GetDelta(Format);

                while (remaining > 0)
                {
                    writer.WriteSamples(tts, 0, tts.Length);

                    remaining -= tts.Length;

                    if (remaining > 0)
                    {
                        int len = Math.Max(BaseSilenceLen + Delta, Min);

                        while (remaining < len)
                        {
                            len--;
                        }

                        short[] silence = new short[len];
                        writer.WriteSamples(silence, 0, silence.Length);

                        Delta += Delta;
                        remaining -= silence.Length;
                    }
                }
            }
        }

        private short[] GetTtsWavData(string phrase)
        {
            string tmp = "tmp.wav";

            using (SpeechSynthesizer sp = new SpeechSynthesizer())
            {
                sp.SelectVoiceByHints(VoiceGender.Female);

                sp.SetOutputToWaveFile(
                    tmp,
                    new SpeechAudioFormatInfo(Settings.Rate, AudioBitsPerSample.Sixteen, AudioChannel.Mono)
                );

                PromptBuilder builder = new PromptBuilder();
                builder.AppendText(phrase);

                sp.Rate = 4;
                sp.Speak(builder);
            }

            short[] WavData;

            using (WaveFileReader reader = new WaveFileReader(tmp))
            {
                var format = reader.WaveFormat;

                byte[] buffer = new byte[reader.Length];
                int read = reader.Read(buffer, 0, buffer.Length);

                WavData = new short[read / 2];
                Buffer.BlockCopy(buffer, 0, WavData, 0, read);
            }

            File.Delete(tmp);

            return WavData;
        }

        private int MsToSamples(int milliseconds, WaveFormat format)
        {
            //Get the number of bytes
            int bytes = (int)(milliseconds * (format.AverageBytesPerSecond / 1000.0));

            //Ensure that the silence length is a multiple of the block align
            bytes -= bytes % format.BlockAlign;

            //Convert from bytes to shorts
            return bytes / 2;
        }

        private int GetDelta(WaveFormat format)
        {
            if (Settings.Increasing)
            {
                return MsToSamples(Settings.DelayDelta, format);
            }
            else if (Settings.Decreasing)
            {
                return -MsToSamples(Settings.DelayDelta, format);
            }
            else
            {
                return 0;
            }
        }
    }
}
