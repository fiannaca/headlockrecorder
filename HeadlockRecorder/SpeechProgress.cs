﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.AudioFormat;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace HeadlockRecorder
{
    /// <summary>
    /// To create the progress feedback when a user is heading straight to a door, execute:
    /// 
    ///     HeadlockRecorder.exe -s=7 -f=ttsdoor.wav -t=true -o=true
    ///     
    /// To create the progress feedback when a user is heading straight to a door but veers off 60% of the way there, execute:
    /// 
    ///     HeadlockRecorder.exe -s=7 -f=ttsdoor.wav -t=true -o=true --pstart=0 --pend=60
    /// </summary>
    public class SpeechProgress : Recorder
    {
        public RecordingSettings Settings { get; set; }

        private int SilenceMs = 1500;

        private WaveFormat Format;

        public SpeechProgress(RecordingSettings s)
        {
            Settings = s;
            Format = new WaveFormat(Settings.Rate, 1);
        }

        public void Record()
        {
            int progress = 0;

            short[] tts = GetTtsWavData("Straight" + progress.ToString());

            using (WaveFileWriter writer = new WaveFileWriter(Settings.Filename, Format))
            {
                int remaining = (int)(Settings.Seconds * (Format.AverageBytesPerSecond / 2.0));

                int SilenceLen = MsToSamples(SilenceMs, Format);

                int increment = (int)(((tts.Length + SilenceLen) / (double)remaining) * (Settings.ProgressEnd - Settings.ProgressStart));

                while (remaining > 0)
                {
                    //Write the TTS
                    writer.WriteSamples(tts, 0, tts.Length);

                    remaining -= tts.Length;

                    progress += increment;
                    tts = GetTtsWavData("Straight" + progress.ToString());

                    //Write the silence
                    if (remaining > 0)
                    {
                        if (remaining < SilenceLen)
                        {
                            SilenceLen -= SilenceLen - remaining;
                        }

                        short[] silence = new short[SilenceLen];
                        writer.WriteSamples(silence, 0, silence.Length);

                        remaining -= silence.Length;
                    }
                }
            }
        }

        private short[] GetTtsWavData(string phrase)
        {
            string tmp = "tmp.wav";

            using (SpeechSynthesizer sp = new SpeechSynthesizer())
            {
                sp.SelectVoiceByHints(VoiceGender.Female);

                sp.SetOutputToWaveFile(
                    tmp,
                    new SpeechAudioFormatInfo(Settings.Rate, AudioBitsPerSample.Sixteen, AudioChannel.Mono)
                );

                PromptBuilder builder = new PromptBuilder();
                builder.AppendText(phrase);

                sp.Rate = 4;
                sp.Speak(builder);
            }

            short[] WavData;

            using (WaveFileReader reader = new WaveFileReader(tmp))
            {
                var format = reader.WaveFormat;

                byte[] buffer = new byte[reader.Length];
                int read = reader.Read(buffer, 0, buffer.Length);

                WavData = new short[read / 2];
                Buffer.BlockCopy(buffer, 0, WavData, 0, read);
            }

            File.Delete(tmp);

            return WavData;
        }

        private int MsToSamples(int milliseconds, WaveFormat format)
        {
            //Get the number of bytes
            int bytes = (int)(milliseconds * (format.AverageBytesPerSecond / 1000.0));

            //Ensure that the silence length is a multiple of the block align
            bytes -= bytes % format.BlockAlign;

            //Convert from bytes to shorts
            return bytes / 2;
        }
    }
}
