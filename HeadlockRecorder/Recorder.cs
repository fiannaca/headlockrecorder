﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadlockRecorder
{
    public interface Recorder
    {
        RecordingSettings Settings { get; set; }

        void Record();
    }
}
