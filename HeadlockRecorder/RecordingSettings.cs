﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadlockRecorder
{
    public enum FeedbackTypes
    {
        Sonification,
        TTS
    }

    public enum FeedbackModes
    {
        DoorFound,
        NoDoor,
        OnTarget,
        OffTarget
    }

    public class RecordingSettings
    {
        public FeedbackTypes Feedback { get; set; }

        public FeedbackModes Mode { get; set; }

        public int Rate { get; set; }

        public double Seconds { get; set; }

        public bool Increasing { get; set; }

        public bool Decreasing { get; set; }

        public int InitialDelay { get; set; }

        public int DelayDelta { get; set; }

        public int MinDelay { get; set; }

        public bool VeeringLeft { get; set; }

        public double FreqStart { get; set; }

        public double FreqEnd { get; set; }

        public double AmpStart { get; set; }

        public double AmpEnd { get; set; }

        public int ProgressStart { get; set; }

        public int ProgressEnd { get; set; }

        public string Filename { get; set; }

        public RecordingSettings()
        {
            //Glissando by default
            Feedback = FeedbackTypes.Sonification;
            Mode = FeedbackModes.OnTarget;

            Increasing = false;
            Decreasing = false;

            InitialDelay = 200;
            DelayDelta = 100;
            MinDelay = 200;
            VeeringLeft = false;

            Rate = 11025;
            Seconds = 5.0;

            FreqStart = 130.0;
            FreqEnd = 1050.0;

            AmpStart = 0.50;
            AmpEnd = 0.05;

            ProgressStart = 0;
            ProgressEnd = 100;

            Filename = "feedback.wav";
        }

        public void SetFeedbackMode(bool? OnTarget = null, bool? DoorFound = null)
        {
            if(OnTarget.HasValue)
            {
                if(OnTarget.Value)
                {
                    Mode = FeedbackModes.OnTarget;
                }
                else
                {
                    Mode = FeedbackModes.OffTarget;
                }
            }
            else if (DoorFound.HasValue)
            {
                if (DoorFound.Value)
                {
                    Mode = FeedbackModes.DoorFound;
                }
                else
                {
                    Mode = FeedbackModes.NoDoor;
                }
            }
            else
            {
                Mode = FeedbackModes.OnTarget;
            }
        }
    }
}
