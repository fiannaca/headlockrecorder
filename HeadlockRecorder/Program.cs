﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;
using NDesk.Options;

namespace HeadlockRecorder
{
    class Program
    {
        static void Main(string[] args)
        {
            RecordingSettings settings = new RecordingSettings();

            bool help = false;

            bool success = false;

            var p = new OptionSet() {
                {"?|help", "Display this message and exit.", 
                    var => help = var != null},
                {"t|tts=", "[All] Specify true for TTS feedback or false for sonification (default = false).",
                    (bool s) => settings.Feedback = !s ? FeedbackTypes.Sonification : FeedbackTypes.TTS},
                {"o|ontarget=", "[All-Guidance] Specify true for on target feedback or false for off target feedback.",
                    (bool s) => settings.SetFeedbackMode(s, null)},
                {"h|hasdoor=", "[All-Discovery] Specify true for door found feedback or false for no door feedback.",
                    (bool s) => settings.SetFeedbackMode(null, s)},
                {"success=", "[All] Specify true if the success feedback should be generated (default = false).",
                    (bool s) => success = s},
                {"i|incr=", "[Sonify-Veer] Specify true if the veering is increasing or false if not (default = false).",
                    (bool s) => settings.Increasing = s},
                {"d|decr=", "[Sonify-Veer] Specify true if the veering is decreasing or false if not (default = false).",
                    (bool s) => settings.Decreasing = s},
                {"delay=", "[All-Veer] Specify the initial amount of delay in the beeping in ms (default = 200).",
                    (int s) => settings.InitialDelay = s},
                {"delta=", "[All-Veer] Specify the amount the delay changes by after each beep (default = 100).",
                    (int s) => settings.DelayDelta = s},
                {"min=", "[All-Veer] Specify the minimum delay between each beep (default = 200).",
                    (int s) => settings.MinDelay = s},
                {"left=", "[Tts-Veer] Specify if the veering direction is to the left (default = false).",
                    (bool s) => settings.VeeringLeft = s},
                {"s|seconds=", "[All] Specify the length of the recording in seconds (float).",
                    (double s) => settings.Seconds = s},
                {"f|filename=", "[All] Specify the filename of the recording (.wav extension).",
                    (string s) => settings.Filename = s},
                {"start=", "[Sonify-Progress] Specify the starting frequency (default = 130.0 Hz).",
                    (double s) => settings.FreqStart = s},
                {"end=", "[Sonify-Progress] Specify the ending frequency (default = 1050.0 Hz).",
                    (double s) => settings.FreqEnd = s},
                {"ampstart=", "[Sonify-Progress] Specify the starting amplitude percent-wise (default = 0.85).",
                    (double s) => settings.AmpStart = s},
                {"ampend=", "[Sonify-Progress] Specify the ending amplitude percent-wise (default = 0.50).",
                    (double s) => settings.AmpEnd = s},
                {"pstart=", "[Tts-Progress] Specify the starting progress percent (default = 0).",
                    (int s) => settings.ProgressStart = s},
                {"pend=", "[Tts-Progress] Specify the ending progress percent (default = 100).",
                    (int s) => settings.ProgressEnd = s},
            };

            try
            {
                p.Parse(args);

                Console.Write("Generating ");
                Console.Write((settings.Mode == FeedbackModes.OnTarget ? "on target " : "off target "));
                Console.Write((settings.Feedback == FeedbackTypes.Sonification ? "sonification " : "text-to-speech "));
                Console.Write("feedback for a period of ");
                Console.Write(settings.Seconds);
                Console.WriteLine(" seconds.");
            }
            catch (OptionException e)
            {
                Console.Write("HeadlockRecorder.exe: ");
                Console.WriteLine(e.Message);
                return;
            }

            if(help)
            {
                Console.WriteLine("Usage: HeadlockRecorder.exe [OPTIONS]");
                Console.WriteLine();
                Console.WriteLine("Options:");

                p.WriteOptionDescriptions(Console.Out);

                return;
            }

            if(success)
            {
                (new SuccessRecorder(settings)).Record();
                return;
            }

            if(settings.Feedback == FeedbackTypes.Sonification)
            {
                switch(settings.Mode)
                {
                    case FeedbackModes.OnTarget:
                        (new GlissRecorder(settings)).Record();
                        break;

                    case FeedbackModes.OffTarget:
                        (new BeepRecorder(settings)).Record();
                        break;

                    default:
                        (new DoorFeedbackRecorder(settings)).Record();
                        break;
                }
            }
            else
            {
                switch (settings.Mode)
                {
                    case FeedbackModes.OnTarget:
                        (new SpeechProgress(settings)).Record();
                        break;

                    case FeedbackModes.OffTarget:
                        (new SpeechVeering(settings)).Record();
                        break;

                    default:
                        (new DoorFeedbackRecorder(settings)).Record();
                        break;
                }
            }
        }
    }
}
